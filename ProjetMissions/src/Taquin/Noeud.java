package Taquin;

import java.util.ArrayList;
import java.util.List;

/**
 * Mod�le de noeud dans le cadre de la r�solution du Taquin Il s'agit d'un
 * mod�le d'arbre standard avec cout o� le nom contient, son �tat, ses fils et
 * les couts associ�s
 * 
 * @author William
 *
 */
public class Noeud {

	public int[][] board;
	public List<Noeud> fils;
	public List<Integer> cout;

	/**
	 * Constructeur de la classe Les couts sont calcul�s par une fonction d�di�e
	 * 
	 * @param board
	 *            - int[][] - Etat du noeud
	 * @param fils
	 *            - List<Noeud> - Liste de ses successeurs
	 */
	public Noeud(int[][] board, List<Noeud> fils) {
		super();
		this.board = board;
		this.fils = fils;
	}

	/**
	 * Fonction qui effectue un d�placement donn� et ajoute le nouveau noeud
	 * fils a la liste de celui-ci
	 * 
	 * @param i
	 *            - int - position en x
	 * @param j
	 *            - int - position en y
	 * @param k
	 *            - int - deplacement en x
	 * @param l
	 *            - int - deplacement en y
	 */
	public void testfils(int i, int j, int k, int l) {
		int[][] nouveauBoard = clone(board);
		int temp = nouveauBoard[i + k][j + l];
		nouveauBoard[i][j] = temp;
		nouveauBoard[i + k][j + l] = 9;
		Noeud configurationSuivante = new Noeud(nouveauBoard, new ArrayList<Noeud>());
		fils.add(configurationSuivante);

	}

	/**
	 * Fonction en charge du remplissage des couts Cette structure de donn�e
	 * serait simplifiable dans le cadre d'une optimisation puisque le cout est
	 * toujours de 1 (i.e. 1 mouvement)
	 */
	public void remplirCout() {
		cout = new ArrayList<>();
		for (int i = 0; i < fils.size(); i++) {
			cout.add(1);
		}
	}

	/**
	 * Fonction de d�couverte des fils du noeud courrant On parcours chaque case
	 * du taquin pour trouver la case 9 qui est la case vide dans notre mod�le
	 * On applique ensuite la fonction de mouvement en fonction de la postion
	 * actuelle
	 */
	public void trouveFils() {
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				if (board[i][j] == 9) {
					// Coin : haut gauche
					if (i == 0 && j == 0) {
						testfils(i, j, 1, 0);
						testfils(i, j, 0, 1);
					}

					// Coin : haut droit
					if (i == 0 && j == (board.length - 1)) {
						testfils(i, j, 1, 0);
						testfils(i, j, 0, -1);
					}

					// Coin : bas gauche
					if (i == (board.length - 1) && j == 0) {
						testfils(i, j, -1, 0);
						testfils(i, j, 0, 1);
					}

					// Coin : bas droit
					if (i == (board.length - 1) && j == (board.length - 1)) {
						testfils(i, j, -1, 0);
						testfils(i, j, 0, -1);
					}

					// Bord haut - Non coin
					if (i == 0 && j != 0 && j != (board.length - 1)) {
						testfils(i, j, 0, -1);
						testfils(i, j, 1, 0);
						testfils(i, j, 0, 1);
					}

					// Bord bas - Non coin
					if (i == (board.length - 1) && j != 0 && j != (board.length - 1)) {
						testfils(i, j, -1, 0);
						testfils(i, j, 0, 1);
						testfils(i, j, 0, -1);
					}

					// Bord gauche - Non coin
					if (j == 0 && i != 0 && i != (board.length - 1)) {
						testfils(i, j, 0, 1);
						testfils(i, j, 1, 0);
						testfils(i, j, -1, 0);
					}

					// Bord droit - Non coin
					if (j == (board.length - 1) && i != 0 && i != (board.length - 1)) {
						testfils(i, j, 1, 0);
						testfils(i, j, -1, 0);
						testfils(i, j, 0, -1);
					}

					// Centre
					if (i != 0 && i != (board.length - 1) && j != 0 && j != (board.length - 1)) {
						testfils(i, j, 0, 1);
						testfils(i, j, -1, 0);
						testfils(i, j, 0, -1);
						testfils(i, j, 1, 0);
					}

				}
			}

		}
	}

	/**
	 * Fonction qui v�rifie case par case que l'�tat du noeud courant est le
	 * m�me que celui pass� en param�tre
	 * 
	 * @param toMatch
	 *            - int[][] - Etat a v�rifier
	 * @return test - boolean - Statut de l'�galit�
	 */
	public boolean equality(int[][] toMatch) {
		boolean test = true;
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				if (board[i][j] != toMatch[i][j]) {
					test = false;
				}
			}
		}
		return test;
	}

	/**
	 * Fonction de clonage case par case d'un �tat - Permet d'�viter les effets
	 * de bords
	 * 
	 * @param toClone
	 *            - Etat a cloner
	 * @return toReturn - int[][] - Etat identique mais de r�f�rence diff�rente
	 */
	public int[][] clone(int[][] toClone) {
		int[][] toReturn = new int[toClone.length][toClone.length];
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				toReturn[i][j] = toClone[i][j];
			}
		}
		return toReturn;
	}

	/**
	 * Fonction d'afficgafe d'un �tat
	 */
	public String toString() {
		String result = "\n";
		for (int[] ligne : board) {
			for (int colonne : ligne) {
				result += colonne + " ";
			}
			result += "\n";
		}
		return result;

	}

	// Les fonctions qui suivent cette ligne sont toutes des getters/setters des
	// diff�rents attributs de cette classe

	public int[][] getBoard() {
		return board;
	}

	public void setBoard(int[][] board) {
		this.board = board;
	}

	public List<Noeud> getFils() {
		return fils;
	}

	public void setFils(List<Noeud> fils) {
		this.fils = fils;
	}

	public List<Integer> getCout() {
		return cout;
	}

	public void setCout(List<Integer> cout) {
		this.cout = cout;
	}

}
