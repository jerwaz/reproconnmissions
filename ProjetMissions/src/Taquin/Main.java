package Taquin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe principale de ce sous-projet Elle impl�mente la m�thode de lancement
 * pourvu de diff�rents jeux d'exemples (comment� ou d�comment les lignes) Elle
 * impl�mente par ailleurs les diff�rents algorithmes de parcours de graphe pour
 * la r�solution du probl�me ATTENTION : Une erreur stackOverflow est possible
 * si la m�moire de votre VM n'est pas suffisante
 * 
 * @author William
 *
 */
public class Main {

	/**
	 * Un jeu d'exemple est compos� d'une configuration initiale et d'une
	 * configuration finale (taquin de taille identique mais r�solu)
	 */
	//public static int[][] configurationInitiale = { { 4, 8, 6 }, { 7, 3, 1 }, { 5, 2, 9 } };
	 //public static int[][] configurationInitiale = {{1,2,3},{4,5,6},{9,7,8}};
	// //(2 �tapes)
	// public static int[][] configurationInitiale = {{9,2,3},{1,5,6},{4,7,8}};
	// //(4 �tapes)
	// public static int[][] configurationInitiale = {{2,5,3},{1,9,6},{4,7,8}};
	// //(6 �tapes)
	//public static int[][] configurationInitiale = {{5,3,9},{2,1,8},{4,6,7}};
	// //(14 �tapes)
	public static int[][] configurationInitiale = {{5,3,8},{2,9,1},{4,6,7}};
	 //public final static int[][] configurationInitiale = {{10 , 17 , 15, 16},
	// {6 , 8 , 4, 11}, { 13, 3 , 14 , 12}, { 5, 2 , 1 , 9}};
	public final static int[][] configurationFinale = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
	// public final static int[][] configurationFinale = {{1 , 2 , 3, 4}, {5 , 6
	// , 7, 8}, { 9, 10 , 11 , 12}, { 13, 14 , 15 , 16}};
	public static Noeud noeudFinale;
	public static Map<String, Boolean> graphe = new HashMap<String, Boolean>();
	public static Map<String, Boolean> graphe3 = new HashMap<String, Boolean>();

	/**
	 * M�thode de lancement du programme, elle donne les r�sultats des trois
	 * algorithmes de parcours et le nombre d'�tape dans le chemin
	 * 
	 * @param args
	 *            - String[] - Arguments standard de la m�thode main
	 */
	public static void main(String[] args) {
		// Initialisation des diff�rents param�tres
		List<Noeud> filsInit = new ArrayList<>();
		Noeud eInit = new Noeud(configurationInitiale, filsInit);
		eInit.trouveFils();
		List<Noeud> filsInit2 = new ArrayList<>();
		Noeud eInit2 = new Noeud(configurationInitiale, filsInit2);
		eInit2.trouveFils();
		List<Noeud> filsInit3 = new ArrayList<>();
		Noeud eInit3 = new Noeud(configurationInitiale, filsInit3);
		eInit3.trouveFils();
		List<Noeud> filsFinale = new ArrayList<>();
		Noeud eFinale = new Noeud(configurationFinale, filsFinale);
		System.out.println("Le noeud initial est le suivant : " + eInit.toString());
		System.out.println("Le noeud final est le suivant : " + eFinale.toString());

		// R�cup�ration et affichage du chemin pour le BFS
		long BFSexectic = System.currentTimeMillis();
		Map<Noeud, Noeud> chemin = BFS(eInit);
		long BFSexectoc = System.currentTimeMillis() - BFSexectic;
		Noeud noeudTemp = noeudFinale;
		boolean test = true;
		int IterationNumber = 0;
		while (test) {
			if (noeudTemp.equals(eInit)) {
				test = false;
			} else {
				IterationNumber += 1;
			}
			noeudTemp = chemin.get(noeudTemp);
		}
		System.out.println("BFS  - Nombre d'�tapes : " + IterationNumber);
		System.out.println("BFS  - Temps d'�x�cution : " + BFSexectoc+" millisecondes");
		
		// R�cup�ration et affichage du chemin pour le DFS
		long DFSexectic = System.currentTimeMillis();
		List<Noeud> chemin2 = DFS(eInit2);
		long DFSexectoc = System.currentTimeMillis() - DFSexectic;
		System.out.println("DFS - Nombre d'�tapes :" + chemin2.size());
		System.out.println("DFS - Temps d'�x�cution : " + DFSexectoc+" millisecondes");


		// R�cup�ration et affichage du chemin pour le A*
		long Aexectic = System.currentTimeMillis();
		List<Noeud> chemin3 = Astar(eInit3, eFinale);
		long Aexectoc = System.currentTimeMillis() - Aexectic;
		System.out.println("A* - Nombre d'�tapes : " + chemin3.size());
		System.out.println("A* - Temps d'�x�cution : " + Aexectoc+" millisecondes");
	}

	/**
	 * Algorithme A* - Vu en cour - Les seuls ajouts sont les m�thodes
	 * permettant de trouver les fils d'un noeud et les couts associ�s
	 * 
	 * @param eInit
	 *            - Noeud - Noeud initial contenant la configuration de d�part
	 * @param eFinale
	 *            - Noeud - Noeud finale contenant la configuration cible
	 * @return
	 */
	public static List<Noeud> Astar(Noeud eInit, Noeud eFinale) {
		List<Noeud> ongoing = new ArrayList<Noeud>();
		ongoing.add(eInit);
		Map<Noeud, Integer> fCost = new HashMap<>();
		Map<String, Integer> lowestCostTo = new HashMap<>();
		lowestCostTo.put(eInit.toString(), 0);
		Map<Noeud, Noeud> bestPrevious = new HashMap<>();
		bestPrevious.put(eInit, null);
		List<Noeud> toReturn = new ArrayList<>();

		while (!ongoing.isEmpty() && toReturn.isEmpty()) {
			Noeud bestNode = minimize_f(ongoing, fCost);
			if (bestNode.equality(eFinale.getBoard())) {
				toReturn = analyse_f(bestPrevious, bestNode);
			}
			bestNode.trouveFils();
			bestNode.remplirCout();
			ongoing.remove(bestNode);
			for (Noeud n : bestNode.getFils()) {
				int newCostToN = lowestCostTo.get(bestNode.toString()) + cost(bestNode, n);
				if (!lowestCostTo.containsKey(n.toString())) {
					lowestCostTo.put(n.toString(), newCostToN);
					bestPrevious.put(n, bestNode);
					fCost.put(n, lowestCostTo.get(n.toString()) + heuristique(n, eFinale));
					ongoing.add(n);
				} else if (newCostToN < lowestCostTo.get(n.toString())) {
					lowestCostTo.replace(n.toString(), newCostToN);
					bestPrevious.replace(n, bestNode);
					fCost.replace(n, lowestCostTo.get(n.toString()) + heuristique(n, eFinale));
				}
			}
		}
		return toReturn;
	}

	/**
	 * Fonction n�cessaire � l'A* pour trouver le Noeud dont le cout est le
	 * moins �l�v�
	 * 
	 * @param ongoing
	 *            - List<Noeud> - Listes des noeuds a contr�ler
	 * @param fCost
	 *            - Map<Noeud,Integer> - Liste des couts associ�s
	 * @return bestNode - Noeud - Noeud de cout le moins �l�v�
	 */
	public static Noeud minimize_f(List<Noeud> ongoing, Map<Noeud, Integer> fCost) {
		Noeud bestNode = ongoing.get(0);
		if (ongoing.size() != 1) {
			for (Noeud n : ongoing) {
				if (fCost.get(n) < fCost.get(bestNode)) {
					bestNode = n;
				}
			}
		}
		return bestNode;
	}

	/**
	 * Fonction n�cessaire a l'A* renvoyant le cour entre deux noeuds
	 * 
	 * @param bestNode
	 *            - Noeud de cout le moins elev�
	 * @param n
	 *            - Noeud source
	 * @return cost - int - Cout de parcours
	 */
	public static int cost(Noeud bestNode, Noeud n) {
		int cost = bestNode.getCout().get(bestNode.getFils().indexOf(n));
		return cost;
	}

	/**
	 * Fonction n�cessaire � l'A* retrouvant le chemin pour atteindre un noeud
	 * donn�
	 * 
	 * @param bestPrevious
	 *            - Map<Noeud,Noeud> - Map des noeuds associ�s a leur meilleure
	 *            pr�decesseur
	 * @param bestNode
	 *            - Noeud - Le Noeud de d�part de l'analyse
	 * @return List<Noeud> - Chemin pour atteindre le "bestNode"
	 */
	public static List<Noeud> analyse_f(Map<Noeud, Noeud> bestPrevious, Noeud bestNode) {
		List<Noeud> path = new ArrayList<>();
		Noeud currentNode = bestNode;
		boolean test = false;
		while (!test) {
			if (bestPrevious.get(currentNode) == null) {
				test = true;
			} else {
				path.add(bestPrevious.get(currentNode));
				currentNode = bestPrevious.get(currentNode);
			}
		}
		return path;
	}

	/**
	 * Heuristique n�cessaire � l'A*, elle renvoie le nombre de case encore mal
	 * plac� par comparaison avec le noeud cible
	 * 
	 * @param n
	 *            - Noeud - Noeud courant
	 * @param efinale
	 *            - Noeud - Noeud cible
	 * @return mismatch - int - Nombre de case mal plac�es
	 */
	public static int heuristique(Noeud n, Noeud efinale) {
		int mismatch = 0;
		for (int i = 0; i < n.getBoard().length; i++) {
			for (int j = 0; j < n.getBoard().length; j++) {
				if (n.getBoard()[i][j] != efinale.getBoard()[i][j]) {
					mismatch += 1;
				}
			}
		}
		return mismatch;
	}

	/**
	 * Algorithme DFS - vu en cours Notons que la configuration finale est donn�
	 * par l'attribut du d�but de la classe et non par un param�tre dans ce cas
	 * !
	 * 
	 * @param eInit
	 *            - Noeud - Noeud de d�part
	 * @return seq - List<Noeud> - Suite de noeud pour all� du noeud de d�part a
	 *         la configuration finale
	 */
	public static List<Noeud> DFS(Noeud eInit) {
		List<Noeud> seq = new ArrayList<Noeud>();
		HashMap<String, Boolean> visited = new HashMap<>();
		explore(seq, visited, eInit);
		return seq;
	}

	/**
	 * Fonction d'exploration n�cessaire au DFS - vu en cours
	 * 
	 * @param seq
	 *            - List<Noeud> - Liste de noeud qui contiendra le chemin
	 * @param visited
	 *            - List<Noeud> - Liste des noeuds deja visit� par l'algorithme
	 * @param n
	 *            - Noeud - Noeud de lancement de l'algorithme
	 */
	public static void explore(List<Noeud> seq, HashMap<String, Boolean> visited, Noeud n) {
		seq.add(n);
		visited.put(n.toString(), true);
		if (!n.equality(configurationFinale)) {
			n.trouveFils();
			for (Noeud node : n.getFils()) {
				if (!(seq.get(seq.size() - 1)).equality(configurationFinale) && !visited.containsKey(node.toString())) {
					explore(seq, visited, node);
				}
			}
		}
		if (!(seq.get(seq.size() - 1)).equality(configurationFinale)) {
			seq.remove(seq.size() - 1);
		}
	}

	/**
	 * Algorithme BFS - vu en cours Notons que la configuration finale et le
	 * graphe construit sont donn�s par les attributs du d�but de la classe et
	 * non par des param�tres dans ce cas !
	 * 
	 * @param eInit
	 *            - Noeud - Noeud d'initialisation de l'algorithme
	 * @return pred - Map<Noeud,Noeud> - Map des noeuds et de leurs
	 *         pr�d�cesseurs conserv� par le BFS
	 */
	public static Map<Noeud, Noeud> BFS(Noeud eInit) {
		// Initialisation
		List<Noeud> queue = new ArrayList<Noeud>();
		Map<Noeud, Noeud> pred = new HashMap<Noeud, Noeud>();
		queue.add(eInit);
		graphe.put(eInit.toString(), true);
		pred.put(eInit, null);
		boolean test = false;

		// Bouclage
		while (queue.size() != 0 && !test) {
			Noeud eCurrent = queue.get(0);
			queue.remove(0);
			eCurrent.trouveFils();
			for (Noeud fils : eCurrent.getFils()) {
				if (!graphe.containsKey(fils.toString())) {
					graphe.put(fils.toString(), true);
					queue.add(fils);
					pred.put(fils, eCurrent);
					if (fils.equality(configurationFinale)) {
						test = true;
						noeudFinale = fils;
					}
				}
			}
		}
		return pred;
	}

}
