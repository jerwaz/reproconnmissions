package MissionThird;

/**
 * Classe g�n�rique du mod�le d'unit� dans un but d'�largissement de l'outil Une
 * unit� g�n�rique est caract�ris�e d'un nom qui fait office d'identifiant,
 * d'une positon en x sur la grille, d'une position en y sur la grille et d'un
 * �tat (morte ou vivante)
 * 
 * @author William
 *
 */
public abstract class Unite {

	public String name;
	public int posX;
	public int posY;
	public String status;

	/**
	 * Constructeur g�n�rique d'une unit�
	 * 
	 * @param name
	 *            - String - Nom de l'unit�
	 * @param posX
	 *            - int - Position en x
	 * @param posY
	 *            - int - Position en y
	 */
	public Unite(String name, int posX, int posY) {
		super();
		this.name = name;
		this.posX = posX;
		this.posY = posY;
		this.status = "alive";
	}

	/**
	 * Fonction renvoyant le nombre de mouvements possibles pour l'unit� h�rit�e
	 * 
	 * @return nbMoveAvailable - int - Nombre de mouvements possibles
	 */
	public abstract int getNbMoveAvailable();

	/**
	 * Fonction renvoyant le nombre de tirs possibles pour l'unit� h�rit�e
	 * 
	 * @return nbShotAvailable - int - Nombre de tirs possibles
	 */
	public abstract int getNbShotAvailable();

	/**
	 * Fonction renvoyant le masque de mouvement de l'unit� h�rit�e
	 * 
	 * @return nbMoveAvailable - int[][] - Masque de mouvement
	 */
	public abstract int[][] getMoveAvailable();

	/**
	 * Fonction renvoyant le masque de tir dans le sens standard de l'unit�
	 * h�rit�e
	 * 
	 * @return nbMoveAvailable - int[][] - masque de tir dans le sens standard
	 */
	public abstract int[][] getShootAvailable();

	/**
	 * Fonction renvoyant le masque de tir dans le sens inverse de l'unit�
	 * h�rit�e
	 * 
	 * @return nbMoveAvailable - int[][] - masque de tir dans le sens inverse
	 */
	public abstract int[][] getShootAvailableRevert();

	/**
	 * Fonction de duplication d'une id�e pour �viter les effets de bords
	 * 
	 * @return clone - Unite - Unite identique mais de r�f�rence diff�rente
	 */
	public abstract Unite duplicate();

	// Les fonctions qui suivent cette ligne sont toutes des getters/setters des
	// diff�rents attributs de cette classe

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public void dead() {
		status = "dead";
	}

	public boolean isDead() {
		return status.equals("dead");
	}

}