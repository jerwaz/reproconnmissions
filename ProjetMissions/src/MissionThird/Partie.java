package MissionThird;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe principal permettant de lancer les tests. Les grilles accueillant les unites sont a d�clar� dans les attributs
 * en ne contenant que des null mais de la taille d�sir�e. Les diff�rents unit�s sont a d�clar�s dans le main. Leur ajou
 * au liste et a la grille d'unit� dout aussi �tre effectu� depuis le main.
 * @author William
 *
 */
public class Partie {
	
	/**public static String[][] currentConfigurationPrint = {{null,null,null,null,null},
									  {null,null,null,null,null},
									  {null,null,null,null,null},
									  {null,null,null,null,null},
									  {null,null,null,null,null}};*/
	public static String[][] currentConfigurationPrint = {{null,null,null,null,null},
			  {null,null,null,null,null},
			  {null,null,null,null,null},
			  {null,null,null,null,null},
			  {null,null,null,null,null},
			  {null,null,null,null,null}};
	/**public static Unite[][] currentConfigurationUnit = {{null,null,null,null,null},
			  {null,null,null,null,null},
			  {null,null,null,null,null},
			  {null,null,null,null,null},
			  {null,null,null,null,null}};*/
	public static Unite[][] currentConfigurationUnit = {{null,null,null,null,null},
			  {null,null,null,null,null},
			  {null,null,null,null,null},
			  {null,null,null,null,null},
			  {null,null,null,null,null},
			  {null,null,null,null,null}};;
	
			  
	/**
	 * M�thode principal lancant une partie et ne s'arretant que lorsque l'une des deux IA a tu� son adversaire.
	 * @param args - Param�tre standard de la m�thode main
	 */
	public static void main(String[] args){
		//D�claration et ajout des unit�s IARandom
		Mitrailleurs mitrRand = new Mitrailleurs("MR",1,1);
		Fantassins fantRand = new Fantassins("FR",1,2);
		Tours tourRand = new Tours("TR",1,3);
		//Mitrailleurs mitrRand2 = new Mitrailleurs("MR2",1,4);
		List<Unite> unitesRand = new ArrayList<>();
		unitesRand.add(mitrRand);
		unitesRand.add(fantRand);
		unitesRand.add(tourRand);
		//unitesRand.add(mitrRand2);
		//Mitrailleurs mitr = new Mitrailleurs("M",3,1);
		//Fantassins fant = new Fantassins("F",3,2);
		//Tours tour = new Tours("T",3,3);
		
		//D�claration et ajout des unit�s IAMinimax
		Mitrailleurs mitr = new Mitrailleurs("M",4,1);
		Fantassins fant = new Fantassins("F",4,2);
		Tours tour = new Tours("T",4,3);
		List<Unite> unites = new ArrayList<>();
		unites.add(mitr);
		unites.add(fant);
		unites.add(tour);
		
		//Ajout a la grille et afficghage
		currentConfigurationUnit[mitr.getPosX()][mitr.getPosY()] = mitr;
		currentConfigurationUnit[mitrRand.getPosX()][mitrRand.getPosY()] = mitrRand;
		currentConfigurationUnit[fant.getPosX()][fant.getPosY()] = fant;
		currentConfigurationUnit[fantRand.getPosX()][fantRand.getPosY()] = fantRand;
		currentConfigurationUnit[tour.getPosX()][tour.getPosY()] = tour;
		currentConfigurationUnit[tourRand.getPosX()][tourRand.getPosY()] = tourRand;
		//currentConfigurationUnit[mitrRand2.getPosX()][mitrRand2.getPosY()] = mitrRand2;
		refresh();
		print();
		Noeud initial = new Noeud(currentConfigurationUnit,unites,unitesRand);
		
		//Variables de mesures
		int randomAverage = 0;
		int minimaxAverage = 0;
		int nbTurn = 0;
		
		//On donne la priorit� a l'IARandom et on lance la partie
		//Chaque cas de filtrage est un mirroir de l'autre en appelant cependant les fonctions de noeuds correspondant a l'IA
		int player = 1;
		Noeud currentnode = initial;
		Noeud nextnode = null;
		while(currentnode.getUnites().size() != 0 && currentnode.getUnitesRand().size() != 0){
			if(player == 1){
				long tic = System.currentTimeMillis();
				System.out.println("Random : \n");
				IARandom iar = new IARandom();
				nextnode = iar.randomAction(currentnode);
				currentConfigurationUnit = nextnode.currentConfiguration;
				player = 2;
				long toc = System.currentTimeMillis() - tic;
				randomAverage += toc;
				nbTurn += 1;
				System.out.println("Dur�e de r�flexion de l'IA random : "+toc+ " millisecondes");
				refresh();
				print();
			} else if (player == 2){
				long tic = System.currentTimeMillis();
				System.out.println("Mini : \n");
				IAMinimax iam = new IAMinimax();
				nextnode = iam.randomAction(currentnode);
				currentConfigurationUnit = nextnode.currentConfiguration;
				player = 1;
				long toc = System.currentTimeMillis() - tic;
				minimaxAverage += toc;
				nbTurn += 1;
				System.out.println("Dur�e de r�flexion de l'IA minimax : "+toc+ " millisecondes");
				refresh();
				print();
			}
			currentnode = nextnode;
		}
		System.out.println("Dur�e moyenne de r�flexion pour l'IARandom : "+(randomAverage/nbTurn));
		System.out.println("Dur�e moyenne de r�flexion pour l'IAMinimax : "+(minimaxAverage/nbTurn));
		if(currentnode.getUnites().size() == 0){
			System.out.println("Random win");
		} else {
			System.out.println("Mini win");
		}
		
	}
	
	/**
	 * M�thode de rafraichissemeent de la grille d'affichage par parcours de la grille d'unit�
	 */
	public static void refresh(){
		for(int i = 0; i<currentConfigurationUnit.length;i++){
			for(int j = 0; j<currentConfigurationUnit[i].length;j++){
				if(currentConfigurationUnit[i][j] != null){
					currentConfigurationPrint[i][j] = currentConfigurationUnit[i][j].getName();
				} else {
					currentConfigurationPrint[i][j] = null;
				}
			}
		}
	}
	
	/**
	 * M�thode de rafraichissement qui supprime les unit�s mortes de la grille d'unit�
	 * ATTENTION : Cette fonction n'est au final pas utilis�e dans les algorithmes mais 
	 * pourrait s'av�rer utile en cas de refactoring
	 * @param unites
	 */
	public static void refreshStatus(List<Unite> unites){
		for(int i=0;i<unites.size();i++){
			if(unites.get(i).isDead()){
				Unite toRemove = unites.get(i);
				unites.remove(toRemove);
				i=i-1;
			}
		}
	}
	
	/**
	 * Fonction d'affichage de la grille d'affichage
	 */
	public static void print(){
		for(int i =0;i<currentConfigurationPrint.length;i++){
			for(int j =0;j<currentConfigurationPrint[i].length;j++){
				System.out.print(currentConfigurationPrint[i][j]+" | ");
			}
			System.out.print("\n");
		}
		System.out.println("_____________________");
	}
	

}
