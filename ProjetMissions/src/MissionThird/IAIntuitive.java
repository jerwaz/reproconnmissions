package MissionThird;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe de gestion de l'IAIntuitive qui va contenir les algorithmes de choix via :
 * Une profondeur maximale correspondant au nombre de coup anticip�
 * Une liste de configuration fille / cout parmi lesquelles choisir (coup suivant)
 * Une liste de configuration a la profondeur indiqu� qui permettra, en remontant au parent, de choisir le coup suivant
 * @author William
 *
 */
public class IAIntuitive {
	
	public List<Pair<Noeud,Integer>> configToChoose;
	public List<List<Noeud>> configSuivantes;
	public int maxDepth;
	
	/**
	 * Constructeur de la classe
	 */
	public IAIntuitive (){
		this.initialize();
	}
	
	/**
	 * M�thode d'initialisation des attributs ou l'on r�gle notamment la pronfondeur d'anticipation
	 */
	private void initialize(){
		this.maxDepth = 2;
		this.configToChoose = new ArrayList<>();
		this.configSuivantes = new ArrayList<>();
		for(int i = 0; i< maxDepth -1; i++){
			this.configSuivantes.add(new ArrayList<Noeud>());
		}
	}

	
	/**
	 * M�thode mal nomm�e /!\ Renvoie le fils d�termin� a l'air du DFS
	 * @param n
	 * @return
	 */
	public Noeud randomAction(Noeud n) {
		this.initialize();
		DFS(n);
//		int random = (int) Math.random() * configToChoose.size();
//		return configToChoose.get(random).getL();
		Pair<Noeud,Integer> pairMax = configToChoose.get(0);
		for(Pair<Noeud,Integer> config: configToChoose ){
			if(config.getR() > pairMax.getR()){
				pairMax = config;
			}
		}
		return pairMax.getL();
	}

	/**
	 * DFS largement simplifi� puisqu'on son but va etre de g�n�rer toutes les configurations a maxDepth coups d'avance
	 * @param eInit
	 */
	public void DFS(Noeud eInit) {
		int currentDepth = 0;
		explore(eInit, currentDepth, 0, null);
	}
	
	/**
	 * M�thode explore utilis� par le DFS
	 * @param n
	 * @param currentDepth
	 * @param valMove
	 * @param Original
	 */
	public void explore(Noeud n, int currentDepth, int valMove, Pair<Noeud,Integer> Original) {
		if (currentDepth == 1) {
			Pair<Noeud,Integer> toAdd = new Pair<>(n,n.getValMove());
			Original = toAdd;
			configToChoose.add(toAdd);
		} else if (currentDepth <= maxDepth && currentDepth > 1) {
			configSuivantes.get(currentDepth-2).add(n);
		}
		if (currentDepth < maxDepth) {
			n.trouveFils();
			for (Noeud nf : n.getFils()) {
				if(currentDepth%2 == 1){
					explore(nf, currentDepth + 1, valMove - nf.getValMove(), Original);
				} else {
					explore(nf, currentDepth + 1, valMove + nf.getValMove(), Original);
				}
				
			}
		} else {
			Original.setR(Math.max(Original.getR(), valMove));
		}
	}

}