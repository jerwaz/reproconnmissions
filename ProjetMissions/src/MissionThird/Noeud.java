package MissionThird;

import java.util.ArrayList;
import java.util.List;

/**
 * Mod�le de noeud pour la r�solution de ce probl�me.
 * Un noeud est donc caract�ris� par son �tat (i.e. l'�tat de la grille d'unit�), sa valeur moyenne, la liste des unit�s restantes de l'IAMinimax, 
 * la liste des unit�s restante de l'IARandom et enfin la liste de ses noeuds fils
 * @author William
 *
 */
public class Noeud {

	public Unite[][] currentConfiguration;
	public int valMove;
	public List<Unite> unites;
	public List<Unite> unitesRand;
	public List<Noeud> fils = new ArrayList<>();
	public Noeud pere;
	
	/**
	 * Constructeur de la classe 
	 * @param currentConfiguration - int[][] - Grille des unit�s
	 * @param unites - List<Unite> - Liste des unit�s de l'IAMinimax
	 * @param unitesRand - List<Unite> - Liste des unit�s de l'IARandom 
	 */
	public Noeud(Unite[][] currentConfiguration, List<Unite> unites, List<Unite> unitesRand) {
		this.currentConfiguration = currentConfiguration;
		this.unites = unites;
		this.valMove = 0;
		this.unitesRand = unitesRand;
	}
	
	/**
	 * Constructeur secondaire avec affection de la valeur moyenne
	 * @param currentConfiguration - int[][] - Grille des unit�s
	 * @param unites - List<Unite> - Liste des unit�s de l'IAMinimax
	 * @param unitesRand - List<Unite> - Liste des unit�s de l'IARandom 
	 * @param valMove - int - Valeur moyenne du noeud
	 */
	public Noeud(Unite[][] currentConfiguration, List<Unite> unites, List<Unite> unitesRand, int valMove) {
		this.currentConfiguration = currentConfiguration;
		this.unites = unites;
		this.valMove = valMove;
		this.unitesRand = unitesRand;
	}
	
	/**
	 * Fonction de g�n�ration de l'ensemble des fils du noeud dans le cadre de l'IAMinimax
	 * Celle-ci va parcourir l'ensemble des unit�s restantes pour l'IAMinimax et pour chacune va g�n�rer :
	 * L'ensemble des fils pour le cas ou seul un mouvement est effectu�
	 * L'ensemble des fils pour le cas ou seul un tir est effectu�
	 * L'ensemble des fils pour le ca combinaison des deux
	 * L'application des mouvements et des tirs sont g�r�es par les fonctions d'applications des masques.
	 */
	public void trouveFils() {
		for (Unite u : unites) {
			if (!u.isDead()) {
				// G�n�ration des fils pour le cas du mouvement
				// Les duplications sont n�cessaires pour �viter les effets de bords
				for (int i = 0; i < u.getMoveAvailable().length; i++) {
					for (int j = 0; j < u.getMoveAvailable()[i].length; j++) {
						if (u.getMoveAvailable()[i][j] != 0) {
							List<Unite> unitesFils = new ArrayList<>();
							List<Unite> unitesRandFils = new ArrayList<>();
							unitesFils.addAll(unites);
							unitesRandFils.addAll(unitesRand);
							Unite[][] fils = moveMask(u, currentConfiguration,unitesFils, i, j);
							// ATTENTION : Filtrage n�cessaire puisque dans le cas ou le mouvement n'�tait pas possible, l'application du masque 
							// de mouvement renvoie null
							if (fils != null) {		
								Noeud filsNoeud = new Noeud(fils, unitesFils, unitesRandFils);
								this.fils.add(filsNoeud);
							}
						}
					}
				}

				// G�n�ration des fils pour le cas du tir 
				// Les duplications sont n�cessaires pour �viter les effets de bords
				for (int i = 0; i < u.getShootAvailable().length; i++) {
					for (int j = 0; j < u.getShootAvailable()[i].length; j++) {
						if (u.getShootAvailable()[i][j] != 0) {
							List<Unite> unitesFils = new ArrayList<>();
							List<Unite> unitesRandFils = new ArrayList<>();
							unitesFils.addAll(unites);
							unitesRandFils.addAll(unitesRand);
							Unite[][] fils = shotMask(u,currentConfiguration,unitesRandFils,unitesFils,i,j);
							// ATTENTION : Filtrage n�cessaire puisque dans le cas ou le mouvement n'�tait pas possible, l'application du masque 
							// de tir renvoie null
							if (fils != null) {	
							Noeud filsNoeud = new Noeud(fils, unitesFils, unitesRandFils, 1);
							this.fils.add(filsNoeud);
							}
						}
					}
				}
				
				// G�n�ration des fils pour le cas de la combinaison
				// Les doubles duplications sont n�cessaires pour �viter les effets de bords
				for (int i = 0; i < u.getMoveAvailable().length; i++) {
					for (int j = 0; j < u.getMoveAvailable()[i].length; j++) {
						if (u.getMoveAvailable()[i][j] != 0) {
							List<Unite> unitesFils = new ArrayList<>();
							List<Unite> unitesRandFils = new ArrayList<>();
							unitesFils.addAll(unites);
							unitesRandFils.addAll(unitesRand);
							Unite[][] fils = moveMask(u, currentConfiguration,unitesFils, i, j);
							
							
						
							if (fils != null) {
								// ATTENTION : Filtrage n�cessaire puisque dans le cas ou le mouvement n'�tait pas possible, l'application du masque 
								// de mouvement renvoie null
								// On doit de plus r�cup�rer l'unit� �quivalent a celle en cour de parcours puisque la r�f�rence a �t� modifi�e
								// par l'application du masque de mouvement
								Unite uRef = null;
								for(int ui =0;ui<fils.length;ui++){
									for(int v = 0;v<fils[ui].length;v++){										
										if(fils[ui][v] != null){
											if(fils[ui][v].getName().equals(u.getName())){
											uRef = fils[ui][v];
										}
									}		
								}
								}
								for (int k = 0; k < uRef.getShootAvailable().length; k++) {
									for (int l = 0; l < uRef.getShootAvailable()[k].length; l++) {
										if (uRef.getShootAvailable()[k][l] != 0) {
										List<Unite> unitesFils2 = new ArrayList<>();
										List<Unite> unitesRandFils2 = new ArrayList<>();
										unitesFils2.addAll(unitesFils);
										unitesRandFils2.addAll(unitesRandFils);
											Unite[][] fils2 = shotMask(uRef,fils,unitesRandFils2,unitesFils2,k,l);
											// ATTENTION : Filtrage n�cessaire puisque dans le cas ou le mouvement n'�tait pas possible, l'application du masque 
											// de tir renvoie null
											if (fils2 != null) {	
											Noeud filsNoeud = new Noeud(fils2, unitesFils2, unitesRandFils2, 1);
											this.fils.add(filsNoeud);
											}
										}
									}
								}
							}
						}
					}
				}

			}
		}
	}
	
	/**
	 * Fonction de g�n�ration de l'ensemble des fils du noeud dans le cadre de l'IARandom
	 * IL S'AGIT D'UN MIRROIR DE LA FONCTION trouveFils(), elle ne sera de faites pas comment�e
	 */
	public void trouveFilsRand() {
		for (Unite u : unitesRand) {
			// Trois temps : g�n�ration des moves uniques, des shots unique puis
			// des deux
			if (!u.isDead()) {
				// Temps 1
				for (int i = 0; i < u.getMoveAvailable().length; i++) {
					for (int j = 0; j < u.getMoveAvailable()[i].length; j++) {
						List<Unite> unitesFils = new ArrayList<>();
						List<Unite> unitesRandFils = new ArrayList<>();
						unitesFils.addAll(unites);
						unitesRandFils.addAll(unitesRand);
						if (u.getMoveAvailable()[i][j] != 0) {
							Unite[][] fils = moveMask(u, currentConfiguration,unitesRandFils, i, j);
							if (fils != null) {
								Noeud filsNoeud = new Noeud(fils, unitesFils, unitesRandFils);
								this.fils.add(filsNoeud);
							}
						}
					}
				}

				// Temps 2
				for (int i = 0; i < u.getShootAvailableRevert().length; i++) {
					for (int j = 0; j < u.getShootAvailableRevert()[i].length; j++) {
						if (u.getShootAvailable()[i][j] != 0) {							
							List<Unite> unitesFils = new ArrayList<>();
							List<Unite> unitesRandFils = new ArrayList<>();
							unitesFils.addAll(unites);
							unitesRandFils.addAll(unitesRand);
							Unite[][] fils = shotMaskRevert(u,currentConfiguration,unitesRandFils,unitesFils,i,j);
							if (fils != null) {
							Noeud filsNoeud = new Noeud(fils, unitesFils, unitesRandFils, 1);
							this.fils.add(filsNoeud);
							}
						}
					}
				}
			}
			
			// Temps 3
			for (int i = 0; i < u.getMoveAvailable().length; i++) {
				for (int j = 0; j < u.getMoveAvailable()[i].length; j++) {
					if (u.getMoveAvailable()[i][j] != 0) {
						List<Unite> unitesFils = new ArrayList<>();
						List<Unite> unitesRandFils = new ArrayList<>();
						unitesFils.addAll(unites);
						unitesRandFils.addAll(unitesRand);
						Unite[][] fils = moveMask(u, currentConfiguration,unitesRandFils, i, j);
										
						if (fils != null) {
							Unite uRef = null;
							for(int ui =0;ui<fils.length;ui++){
								for(int v = 0;v<fils[ui].length;v++){
									if(fils[ui][v] != null){
										if(fils[ui][v].getName().equals(u.getName())){
										uRef = fils[ui][v];
									}
								}		
							}
								
							}
							for (int k = 0; k < uRef.getShootAvailableRevert().length; k++) {
								for (int l = 0; l < uRef.getShootAvailableRevert()[k].length; l++) {
									if (uRef.getShootAvailable()[k][l] != 0) {
										List<Unite> unitesFils2 = new ArrayList<>();
										List<Unite> unitesRandFils2 = new ArrayList<>();
										unitesFils2.addAll(unitesFils);
										unitesRandFils2.addAll(unitesRandFils);
										Unite[][] fils2 = shotMaskRevert(uRef,fils,unitesRandFils2,unitesFils2,k,l);
										if (fils2 != null) {	
										Noeud filsNoeud = new Noeud(fils2, unitesFils2, unitesRandFils2, 1);
										this.fils.add(filsNoeud);
										}
									}
								}
							}
						}
					}
				}
			}

		}
	}
	
	/**
	 * Fonction d'application du masque de mouvement
	 * Il suffit de remarquer qu'une translation de r�f�rentiel entre le masque et la grille nous donne le d�placement
	 * On l'applique alors si celui ci est possible
	 * @param utc - Unite - Unit� en cour de parcours qu'il faudra dupliquer
	 * @param config - int[][] - grille des unit�s 
	 * @param toModify - List<Noeud> - Liste d'unit� qu'il va faloir mettre a jour en raison de la duplication
	 * @param x - int - Position dans le masque de mouvement en x
	 * @param y - int - Position dans le masque de mouvement en y
	 * @return configFinal - Grille d'unit� modifi�e ou null si inapplicable
	 */
	public Unite[][] moveMask(Unite utc, Unite[][] config, List<Unite> toModify, int x, int y) {		
		Unite[][] configFinal = new Unite[config.length][config[0].length];
		for(int i =0;i<configFinal.length;i++){
			for(int j = 0;j<configFinal[i].length;j++){
				configFinal[i][j]  =config[i][j];
			}
		}
		Unite u = utc.duplicate();
		int xMid = (int) u.getMoveAvailable().length / 2;
		int yMid = (int) u.getMoveAvailable()[0].length / 2;
		int dplctX = xMid - x;
		int dplctY = yMid - y;
		//Filtrage qui v�rifie que le mouvement reste dans la grille et qu'une unit� n'est pas d�ja pr�sente sur la case
		if (u.getPosX() - dplctX >= 0 && u.getPosY() - dplctY >= 0 && u.getPosX() - dplctX < config.length
				&& u.getPosY() - dplctY < config[0].length) {
			if(configFinal[u.getPosX() - dplctX][u.getPosY() - dplctY] == null){
			configFinal[u.getPosX()][u.getPosY()] = null;
			configFinal[u.getPosX() - dplctX][u.getPosY() - dplctY] = u;
			u.setPosX(u.getPosX() - dplctX);
			u.setPosY(u.getPosY() - dplctY);
			toModify.remove(utc);
			toModify.add(u);
			return configFinal;
			} else  {
				return null;
			}
		} else {
			return null;
		}
	}
	
	/**
	 * Fonction d'application du masque de tir pour l'IAMinimax
	 * Il suffit de remarquer qu'une translation de r�f�rentiel entre le masque et la grille nous donne la distance de tir
	 * On l'applique alors si celui ci est possible (Le friendly fire est interdit)
	 * @param u - Unite - Unite en cour de parcour
 	 * @param config - int[][] - Grille des unit�s
	 * @param unitesRand - List<Unite> - Liste des unites de l'IARandom
	 * @param unites - List<Unite> - Liste des unites de l'IAMinimax
	 * @param x - int - Position dans le masque de tir en x
	 * @param y - int - Position dans le masque de tir en y
	 * @return configFinal - Grille d'unit� modifi�e ou null si inapplicable
	 */
	public Unite[][] shotMask(Unite u, Unite[][] config, List<Unite> unitesRand, List<Unite> unites, int x, int y) {
		Unite[][] configFinal = new Unite[config.length][config[0].length];
		for(int i =0;i<configFinal.length;i++){
			for(int j = 0;j<configFinal[i].length;j++){
				configFinal[i][j]  =config[i][j];
			}
		}
		int xMid = (int) u.getShootAvailable().length / 2;
		int yMid = (int) u.getShootAvailable()[0].length / 2;
		int distX = xMid - x;
		int distY = yMid - y;
		//Filtrage qui v�rifie que le tir reste dans la grille et qu'une unit� ami n'est pas d�ja pr�sente sur la case
		if (u.getPosX() - distX >= 0 && u.getPosY() - distY >= 0 && u.getPosX() - distX < config.length
				&& u.getPosY() - distY < config[0].length) {
			if(configFinal[u.getPosX() - distX][u.getPosY() - distY] != null){
				if(!unites.contains(configFinal[u.getPosX() - distX][u.getPosY() - distY])){
			Unite target = configFinal[u.getPosX() - distX][u.getPosY() - distY];
			configFinal[target.getPosX()][target.getPosY()] = null;
			unitesRand.remove(target);
			return configFinal;
				} else {
					return null;
				}
			} else  {
				return configFinal;
			}
		} else {
			return null;
		}
	}

	/**
	 * Fonction d'application du masque de tir pour l'IARandom
	 * Il suffit de remarquer qu'une translation de r�f�rentiel entre le masque et la grille nous donne la distance de tir
	 * On l'applique alors si celui ci est possible (Le friendly fire est interdit)
	 * @param u - Unite - Unite en cour de parcour
 	 * @param config - int[][] - Grille des unit�s
	 * @param unitesRand - List<Unite> - Liste des unites de l'IARandom
	 * @param unites - List<Unite> - Liste des unites de l'IAMinimax
	 * @param x - int - Position dans le masque de tir en x
	 * @param y - int - Position dans le masque de tir en y
	 * @return configFinal - Grille d'unit� modifi�e ou null si inapplicable
	 */
	public Unite[][] shotMaskRevert(Unite u, Unite[][] config, List<Unite> unitesRand, List<Unite> unites, int x, int y) {
		Unite[][] configFinal = new Unite[config.length][config[0].length];
		for(int i =0;i<configFinal.length;i++){
			for(int j = 0;j<configFinal[i].length;j++){
				configFinal[i][j]  =config[i][j];
			}
		}
		int xMid = (int) u.getShootAvailableRevert().length / 2;
		int yMid = (int) u.getShootAvailableRevert()[0].length / 2;
		int distX = xMid - x;
		int distY = yMid - y;
		//Filtrage qui v�rifie que le tir reste dans la grille et qu'une unit� ami n'est pas d�ja pr�sente sur la case
		if (u.getPosX() - distX >= 0 && u.getPosY() - distY >= 0 && u.getPosX() - distX < config.length
				&& u.getPosY() - distY < config[0].length) {
			if(configFinal[u.getPosX() - distX][u.getPosY() - distY] != null){
				if(!unitesRand.contains(configFinal[u.getPosX() - distX][u.getPosY() - distY])){
			Unite target = configFinal[u.getPosX() - distX][u.getPosY() - distY];
			configFinal[target.getPosX()][target.getPosY()] = null;
			unites.remove(target);
			return configFinal;
				} else {
					return null;
				}
			} else  {
				return configFinal; //situation dut tir dans le vide
			}
		} else {
			return null;
		}
	}
	
	/**
	 * Fonction d'afficgae de la grille des unites du Noeud
	 * Reserve des fins de tests
	 * @param currentConfiguration - Unite[][] - Grille d'unite a afficher
	 */
	public static void printConfig(Unite[][] currentConfiguration){
		for(int i =0;i<currentConfiguration.length;i++){
			for(int j =0;j<currentConfiguration[i].length;j++){
				if(currentConfiguration[i][j] != null){
				System.out.print(currentConfiguration[i][j].getName()+" | ");
				} else {
					System.out.print("null | ");	
				}
			}
			System.out.print("\n");
		}
		System.out.println("_____________________");
	}
	
	// Les fonctions qui suivent cette ligne sont toutes des getters/setters des
	// diff�rents attributs de cette classe
	
	public Unite[][] getCurrentConfiguration() {
		return currentConfiguration;
	}

	public void setCurrentConfiguration(Unite[][] currentConfiguration) {
		this.currentConfiguration = currentConfiguration;
	}

	public List<Unite> getUnites() {
		return unites;
	}

	public void setUnites(List<Unite> unites) {
		this.unites = unites;
	}

	public List<Noeud> getFils() {
		return fils;
	}

	public void setFils(List<Noeud> fils) {
		this.fils = fils;
	}

	public int getValMove() {
		return valMove;
	}

	public void setValMove(int valMove) {
		this.valMove = valMove;
	}	
	
	public List<Unite> getUnitesRand() {
		return unitesRand;
	}

	public void setUnitesRand(List<Unite> unitesRand) {
		this.unitesRand = unitesRand;
	}

	public Noeud getPere() {
		return pere;
	}

	public void setPere(Noeud pere) {
		this.pere = pere;
	}
	
	
}
