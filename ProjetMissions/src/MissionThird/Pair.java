package MissionThird;


/**
 * Simple classe qui permet de stocker des couples d'objets
 * @author Jerome
 * @param <L>
 * @param <R>
 */
public class Pair<L, R> {
	private L l;
	private R r;
	
	  /**
	   * Cr�� un nouvel objet Pair
	   * @param l objet de gauchje de la paire.
	   * @param r objet de droite de la paire
	   */
	
	public Pair(L l, R r) {
		this.l = l;
		this.r = r;
	}

	// Les fonctions qui suivent cette ligne sont toutes des getters/setters des
	// diff�rents attributs de cette classe
	
	public L getL() {
		return l;
	}

	public R getR() {
		return r;
	}

	public void setL(L l) {
		this.l = l;
	}

	public void setR(R r) {
		this.r = r;
	}
}