package MissionThird;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe de gestion de l'IARandom qui ne contient qu'une liste de coup possible en attribut
 * @author William
 *
 */
public class IARandom {
	
	public List<Noeud> configToChoose = new ArrayList<>();
	
	/**
	 * M�thode permettant de remplir la liste des coups possibles
	 * @param n - Noeud - Noeud courrant
	 */
	public void GenereConfig(Noeud n){
		n.trouveFilsRand();
		configToChoose = n.getFils();
	}
	
	/**
	 * M�thode de choix al�atoire
	 * @param n - Noeud - Noeud courrant
	 * @return nextN - Noeud - Noeud choisi al�atoirement
	 */
	public Noeud randomAction(Noeud n){
		GenereConfig(n);
		int random = (int) (Math.random()*configToChoose.size());
		return configToChoose.get(random);
	}
	

}
