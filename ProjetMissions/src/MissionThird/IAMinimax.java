package MissionThird;

/**
 * IA bas�e sur l'algorithme minimax. 
 * 
 * @author Jerome
 * 
 */
public class IAMinimax {

	public int maxDepth;

	/**
	 * Constructeur de la classe
	 */
	public IAMinimax() {
		maxDepth = 3;
	}
	
	/**
	 * M�thode mal nomm�e /!\ Methode prenant en param�tre un noeud et renvoyant le coup jou� par l'IA
	 * @param n
	 * @return
	 */
	public Noeud randomAction(Noeud n) {
		Noeud toReturn = miniMax(0, n).getL();
		for(int i=0; i<maxDepth-1;i++){ // On remonte jusqu'� la profondeur 1, qui correspond au coup jou� par l'IA pour ce tour.
			toReturn = toReturn.getPere();
		}
		return toReturn;
	}

	/**
	 * Methode de parcours minimax. Elle renvoit le meilleur noeud � une profondeur maxDepth 
	 * pour le joueur.
	 * @param depth
	 * @param n
	 * @return
	 */
	public Pair<Noeud, Integer> miniMax(int depth, Noeud n) {
		if (depth == maxDepth) {
			Pair<Noeud, Integer> toReturn = new Pair<>(n, heuristique(n));
			return toReturn;
		} else {
			if (depth % 2 == 1) {
				n.trouveFilsRand();
			}else{
				n.trouveFils();
			}
			if (n.getFils().isEmpty()){
				Pair<Noeud, Integer> toReturn = new Pair<>(n, heuristique(n));
				return toReturn;
			}else{
				Pair<Noeud, Integer> bestValue = null;
				for (Noeud fils : n.getFils()) {
					fils.setPere(n);
					Pair<Noeud, Integer> value = miniMax(depth + 1, fils);
					if (bestValue == null) {
						bestValue = value;
					} else {
						if (depth % 2 == 1) {
							bestValue = minimum(bestValue, value);
						}else{
							bestValue = maximum(bestValue, value);
						}
						
					}
				}
				return bestValue;
			}
			
		}

	}
	/**
	 * M�thode renvoyant le minimum entre deux pairs.
	 * @param bestValue
	 * @param value
	 * @return
	 */
	private Pair<Noeud, Integer> minimum(Pair<Noeud, Integer> bestValue,
			Pair<Noeud, Integer> value) {
		if(bestValue.getR()<value.getR()){
			return bestValue;
		}else{
			return value;
		}
	}

	/**
	 * M�thode renvoyant le maximum entre deux pairs.
	 * @param bestValue
	 * @param value
	 * @return
	 */
	public Pair<Noeud, Integer> maximum(Pair<Noeud, Integer> bestValue,
			Pair<Noeud, Integer> value) {
			if(bestValue.getR()>value.getR()){
				return bestValue;
			}else{
				return value;
			}		
	}
	
	/**
	 * Heuristique basique renvoyant le nombre d'unit�s pour l'IAMinimax
	 * auquel on soustrait le nombre restant d'unit� de IARandom
	 * @param n
	 * @return
	 */
	public int heuristique(Noeud n) {
		return n.getUnites().size() - n.getUnitesRand().size();
	}

}