package MissionThird;

/**
 * Sp�cification d'une unit� en Fantassin avec des masques de tirs et mouvement
 * sp�cifique. Se r�f�rer a la Javadoc d'unit� pour les commentaires
 * 
 * @author William
 *
 */
public class Fantassins extends Unite{
	
	public static final int[][] shootAvailable = {{0,0,1,0,0},
			  {0,0,0,0,0},
			  {0,0,1,0,0},
			  {0,1,0,1,0},
			  {0,0,0,0,0},
			  {0,0,0,0,0}};
	public static final int[][] moveAvailable = {{0,0,0,0,0},
			 {1,0,1,0,1},
			 {0,1,1,1,0},
			 {1,1,0,1,1},
			 {0,1,1,1,0},
			 {1,0,1,0,1}};
	public static final int[][] shootAvailableRevert = {{0,0,1,0,0},
		  {0,0,0,0,0},
		  {0,0,0,0,0},
		  {0,1,0,1,0},
		  {0,0,1,0,0},
		  {0,0,0,0,0}};
	
	public static final int nbMoveAvailable = 16;
	public static final int nbShootAvailable = 3;
	
	public Fantassins(String name, int posX, int posY) {
		super(name, posX, posY);
	}
	
	@Override
	public int getNbMoveAvailable() {
		return nbMoveAvailable;
	}

	@Override
	public int getNbShotAvailable() {
		return nbShootAvailable;
	}
	
	@Override
	public int[][] getMoveAvailable() {
		return moveAvailable;
	}

	@Override
	public int[][] getShootAvailable() {
		return shootAvailable;
	}

	@Override
	public int[][] getShootAvailableRevert() {
		return shootAvailableRevert;
	}
	
	@Override
	public Unite duplicate() {
		return new Fantassins(this.name,this.posX,this.posY);
	}
		
}
