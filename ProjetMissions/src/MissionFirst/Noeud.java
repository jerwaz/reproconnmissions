package MissionFirst;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Noeud {
	//Stockge du Cargo: premier array: les 9 emplacements
	//Les sous-array: les 4 places pour conteneur par emplacement
	public String[][] cargos;
	//Stockage des diff�rents types de conteneurs dans le port:
	//A: Artillerie, V: vivres, M: munition, CL: combat l�ger
	//La valeur d'une cl�f correspond au nombre de conteneurs de ce type
	public HashMap<String, Integer> port;
	//Stockage des fils
	public List<Noeud> fils;
	//Co�t de l'arc entre le noeud et ses fils (mis � 1 pour tous ici � chaque appel du co�t)
	public List<Integer> cout;

	/**
	 * Constructeur
	 * @param cargos
	 * @param port
	 */
	public Noeud(String[][] cargos, HashMap<String, Integer> port) {
		this.cargos = cargos;
		this.port = port;
		this.fils = new ArrayList<>();
	}
	
	/**
	 * Methode de remplissage des co�ts
	 */
	public void remplirCout() {
		cout = new ArrayList<>();
		for (int i = 0; i < fils.size(); i++) {
			cout.add(1);
		}
	}

	//
	/**
	 * M�thode Ajouttant un fils au Noeud
	 * @param i emplacement
	 * @param j �tage de l'emplacement
	 * @param what type de conteneur: "A", "CL", "V", "M"
	 * @param arti Nombre de conteneurs de type A restant dans le port
	 * @param vivre Nombre de conteneurs de type V restant dans le port
	 * @param ammo Nombre de conteneurs de type M restant dans le port
	 * @param combatL Nombre de conteneurs de type CL restant dans le port
	 */
	private void addFils(int i, int j, String what, int arti, int vivre,
			int ammo, int combatL) {
		HashMap<String, Integer> newPort = new HashMap<>();
		newPort.put("A", arti);
		newPort.put("V", vivre);
		newPort.put("M", ammo);
		newPort.put("CL", combatL);

		String[][] newCargos = new String[9][];
		for (int k = 0; k < 9; k++) {
			newCargos[k] = new String[4];
			for (int l = 0; l < 4; l++) {
				if (i == k && j == l) {
					newCargos[k][l] = what;
				} else {
					newCargos[k][l] = this.cargos[k][l];
				}
			}
		}
		this.fils.add(new Noeud(newCargos, newPort));
	}

	/**
	 * Methode de recherche des fils
	 */
	public void trouveFils() {
		int arti = port.get("A");
		int vivre = port.get("V");
		int ammo = port.get("M");
		int combatL = port.get("CL");
		
		//Pour chacun des emplacements
		for (int i = 0; i < 9; i++) {
			//Variable servant d'arret de la boucle sur la pile lorsqu'on arrive 
			//au premier �tage vide de l'emplacement
			boolean breaker = false;
			//Variable devient true si un conteneur contenant des munitions est d�j� stock� sur l'emplacement
			boolean containsAmmo = false;
			for (int j = 0; j < 4; j++) {
				// On s'arr�te si on est en haut de la pile
				if (breaker) {
					break;
				} else if (cargos[i][j] == null) { // On est en haut de la pile
					if (arti == 0 && ammo == 0 && combatL == 0 && vivre != 0) { //On ajoute les vivres seulement � la fin
						addFils(i, j, "V", arti, vivre - 1, ammo, combatL);
					} else {
						if (i == 0 && j < 2) {//Repect de la regle stipulant que l'on doit poser un conteneur d'equipements
											  //de combat l�ger et un conteneur de mlunitions dans le premier emplacement
							if (j == 0) {
								if (ammo != 0) {
									addFils(i, j, "M", arti, vivre, ammo - 1,
											combatL);
								}
								if (combatL != 0) {
									addFils(i, j, "CL", arti, vivre, ammo,
											combatL - 1);
								}
								if (ammo == 0 && combatL == 0) {
									if (arti != 0) {
										addFils(i, j, "A", arti - 1, vivre,
												ammo, combatL);
									}

								}

							} else if (j == 1) {
								if (ammo != 0 && "CL".equals(cargos[0][0])) {
									addFils(i, j, "M", arti, vivre, ammo - 1,
											combatL);
								} else if (combatL != 0
										&& "M".equals(cargos[0][0])) {
									addFils(i, j, "CL", arti, vivre, ammo,
											combatL - 1);
								} else {
									if ("A".equals(cargos[0][0]) && arti != 0) {
										addFils(i, j, "A", arti - 1, vivre,
												ammo, combatL);
									}
									if (combatL != 0) {
										addFils(i, j, "CL", arti, vivre, ammo,
												combatL - 1);
									}
									if (ammo != 0) {
										addFils(i, j, "M", arti, vivre,
												ammo - 1, combatL);
									}
								}
							}
						} else {
							if (arti != 0 && j < 2) {//Respect de la r�gle stipulant que un conteneur d'artillerie
													 //doit �tre en dessous de tous les autres et qu'au maximum
													 //deux conteneurs de ce type peuvent �tre stock�s par emplacement
								if (j == 0) {
									addFils(i, j, "A", arti - 1, vivre, ammo,
											combatL);
								} else if (j == 1 && "A".equals(cargos[i][0])) {
									//Respect de la r�gle stipulant qu'un conteneur d'artillerie peut �tre stock� au 
									//desus d'un autre si et seulement si celui-ci est un conteneur d'artillerie
									addFils(i, j, "A", arti - 1, vivre, ammo,
											combatL);
								}
							}

							if (ammo != 0 && !containsAmmo) { //Respect de la r�gle: Maximum un conteneur de munitions par emplacement
								addFils(i, j, "M", arti, vivre, ammo - 1,
										combatL);
							}
							if (combatL != 0) {
								addFils(i, j, "CL", arti, vivre, ammo,
										combatL - 1);
							}
						}
					}

					breaker = true;

				} else if ("M".equals(cargos[i][j])) {
					containsAmmo = true;
				}

			}

		}

	}

	public String[][] getCargos() {
		return cargos;
	}

	public void setCargos(String[][] cargos) {
		this.cargos = cargos;
	}

	public HashMap<String, Integer> getPort() {
		return port;
	}

	public void setPort(HashMap<String, Integer> port) {
		this.port = port;
	}

	public List<Noeud> getFils() {
		return fils;
	}

	public void setFils(List<Noeud> fils) {
		this.fils = fils;
	}

	public List<Integer> getCout() {
		return cout;
	}

	public void setCout(List<Integer> cout) {
		this.cout = cout;
	}
	
	//
	/**
	 * M�thode testant l'�galit� entre ce noeud et le noeud en param�tre de la m�thode
	 * @param noeud Noeud � comparer
	 * @return
	 */
	public boolean equality(Noeud noeud) {
		boolean test = true;
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 4; j++) {
				if (this.getCargos()[i][j] != null) {
					if (noeud.getCargos()[i][j] != null) {
						if (!this.getCargos()[i][j]
								.equals(noeud.getCargos()[i][j])) {
							test = false;
						}
					} else {
						test = false;
					}
				} else if (noeud.getCargos()[i][j] != null) {
					test = false;
				}
			}
		}
		return test;
	}
	
	/**
	 * Methode testant sur ce noeud est solution ou pas
	 * @return boolean
	 */

	public boolean isSolution() {
		//On suppose que le noeud est juste par construction
		//On ne teste alors que si le port est vide ou pas
		if (this.getPort().get("A") == 0 && this.getPort().get("M") == 0
				&& this.getPort().get("CL") == 0
				&& this.getPort().get("V") == 0) {
			return true;
		}

		return false;
	}
	
	/**
	 * Methode renvoyant un string d�crivant le noeud
	 * @return String
	 */
	
	public String toString() {
		String result = "";
		int i = 0;
		for (String[] emplacement : this.getCargos()) {
			i++;
			result += i + " ";
			for (String container : emplacement) {
				if (container != null) {
					result += container + " ";
				} else {
					break;
				}

			}
			result += "\n";
		}
		result += "\n";
		return result;
	}
	
}
