package MissionFirst;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Jerome
 * 
 */
public class Main {

	public static Noeud noeudFinale;
	public static Map<String, Boolean> graphe = new HashMap<String, Boolean>();

	public static void main(String[] args) {

		// Initialisation du cargos
		String[][] cargos = new String[9][];
		for (int i = 0; i < 9; i++) {
			cargos[i] = new String[4];
		}
		// Initialisation du port
		HashMap<String, Integer> port = new HashMap<>();
		port.put("A", 8); // Artillerie
		port.put("V", 11);// Vivres
		port.put("M", 9);// Munitions
		port.put("CL", 8);// Combat leger

		if (port.get("A") + port.get("V") + port.get("M") + port.get("CL") > 36) {
			System.out
					.println("Il y a trop de conteneurs pour remplir le cargo");
		} else if (port.get("M") > 9) {
			System.out.println("Il y a trop de munitions, max 9");
		} else if (port.get("A") > 18) {
			System.out
					.println("Il y a trop de conteneur d'artillerie, max 18 (2 par emplacement) ");
		} else {

			Noeud eInit = new Noeud(cargos, port);
			long DFSexectic = System.currentTimeMillis();
			List<Noeud> dfs = new ArrayList<>(DFS(eInit));
			long DFSexectoc = System.currentTimeMillis() - DFSexectic;

			// Afficher chemin
			// for(Noeud etat : dfs){
			// System.out.println(etat.toString());
			// }
			System.out.println("Noeud solution:");
			System.out.println(dfs.get(dfs.size() - 1));
			System.out.println("Profondeur DFS: " + dfs.size());
			System.out.println("Temps d'ex�cution DFS: " + DFSexectoc
					+ " millisecondes");

			// BFS beaucoup trop long pour ce probl�me et inadapt� (profondeur
			// des solutions constante)

			// long BFSexectic = System.currentTimeMillis();
			// Map<Noeud, Noeud> chemin = BFS(eInit);
			// long BFSexectoc = System.currentTimeMillis() - BFSexectic;
			// Noeud noeudTemp = noeudFinale;
			// boolean test = true;
			// int IterationNumber = 0;
			// while (test) {
			// if (noeudTemp.equals(eInit)) {
			// test = false;
			// } else {
			// IterationNumber += 1;
			// }
			// noeudTemp = chemin.get(noeudTemp);
			// }
			// System.out.println("BFS  - Nombre d'�tapes : " +
			// IterationNumber);
			// System.out.println("BFS  - Temps d'�x�cution : " +
			// BFSexectoc+" millisecondes");

			long ASTARexectic = System.currentTimeMillis();
			List<Noeud> astar = new ArrayList<>(Astar(eInit));
			long ASTARexectoc = System.currentTimeMillis() - ASTARexectic;

			// Afficher chemin
			// for(Noeud noeud : astar){
			// System.out.println(noeud.toString());
			// }
			System.out.println("Noeud solution:");
			System.out.println(astar.get(0));
			System.out.println("Profondeur A*: " + (astar.size()));
			System.out.println("Temps d'ex�cution A*: " + ASTARexectoc
					+ " millisecondes");
		}
	}

	/**
	 * M�thode de recherche de solution A*
	 * 
	 * @param eInit
	 *            - Noeud - Noeud initial contenant la configuration de d�part
	 * @return List<Noeud> suite d'etats menant � la solution
	 */
	public static List<Noeud> Astar(Noeud eInit) {
		List<Noeud> ongoing = new ArrayList<Noeud>();
		ongoing.add(eInit);
		Map<Noeud, Integer> fCost = new HashMap<>();
		Map<String, Integer> lowestCostTo = new HashMap<>();
		lowestCostTo.put(eInit.toString(), 0);
		Map<Noeud, Noeud> bestPrevious = new HashMap<>();
		bestPrevious.put(eInit, null);
		List<Noeud> toReturn = new ArrayList<>();

		while (!ongoing.isEmpty() && toReturn.isEmpty()) {
			Noeud bestNode = minimize_f(ongoing, fCost);
			if (bestNode.isSolution()) {
				toReturn = analyse_f(bestPrevious, bestNode);
			}
			bestNode.trouveFils();
			bestNode.remplirCout();
			ongoing.remove(bestNode);
			for (Noeud n : bestNode.getFils()) {
				int newCostToN = lowestCostTo.get(bestNode.toString())
						+ cost(bestNode, n);
				if (!lowestCostTo.containsKey(n.toString())) {
					lowestCostTo.put(n.toString(), newCostToN);
					bestPrevious.put(n, bestNode);
					fCost.put(n, lowestCostTo.get(n.toString())
							+ heuristique(n));
					ongoing.add(n);
				} else if (newCostToN < lowestCostTo.get(n.toString())) {
					lowestCostTo.replace(n.toString(), newCostToN);
					bestPrevious.replace(n, bestNode);
					fCost.replace(n, lowestCostTo.get(n.toString())
							+ heuristique(n));
				}
			}
		}
		return toReturn;
	}

	/**
	 * Heuristique n�cessaire � l'A*. Elle renverra une valeur repr�sentant
	 * l'�tat du port.
	 * 
	 * @param n
	 *            - Noeud - Noeud courant
	 * @return mismatch
	 */
	public static int heuristique(Noeud n) {
		int mismatch = 0;
		mismatch += n.getPort().get("A") * 4;
		mismatch += n.getPort().get("M") * 3;
		mismatch += n.getPort().get("CL") * 2;
		mismatch += n.getPort().get("V");

		return mismatch;
	}

	/**
	 * Fonction n�cessaire � l'A* pour trouver le Noeud dont le cout est le
	 * moins �l�v�
	 * 
	 * @param ongoing
	 *            - List<Noeud> - Listes des noeuds a contr�ler
	 * @param fCost
	 *            - Map<Noeud,Integer> - Liste des couts associ�s
	 * @return bestNode - Noeud - Noeud de cout le moins �l�v�
	 */
	public static Noeud minimize_f(List<Noeud> ongoing,
			Map<Noeud, Integer> fCost) {
		Noeud bestNode = ongoing.get(0);
		if (ongoing.size() != 1) {
			for (Noeud n : ongoing) {
				if (fCost.get(n) < fCost.get(bestNode)) {
					bestNode = n;
				}
			}
		}
		return bestNode;
	}

	/**
	 * Fonction n�cessaire a l'A* renvoyant le cour entre deux noeuds
	 * 
	 * @param bestNode
	 *            - Noeud de cout le moins elev�
	 * @param n
	 *            - Noeud source
	 * @return cost - int - Cout de parcours
	 */
	public static int cost(Noeud bestNode, Noeud n) {
		int cost = bestNode.getCout().get(bestNode.getFils().indexOf(n));
		return cost;
	}

	/**
	 * Fonction n�cessaire � l'A* retrouvant le chemin pour atteindre un noeud
	 * donn�
	 * 
	 * @param bestPrevious
	 *            - Map<Noeud,Noeud> - Map des noeuds associ�s a leur meilleure
	 *            pr�decesseur
	 * @param bestNode
	 *            - Noeud - Le Noeud de d�part de l'analyse
	 * @return List<Noeud> - Chemin pour atteindre le "bestNode"
	 */
	public static List<Noeud> analyse_f(Map<Noeud, Noeud> bestPrevious,
			Noeud bestNode) {
		List<Noeud> path = new ArrayList<>();
		path.add(bestNode);
		Noeud currentNode = bestNode;
		boolean test = false;
		while (!test) {
			if (bestPrevious.get(currentNode) == null) {
				test = true;
			} else {
				path.add(bestPrevious.get(currentNode));
				currentNode = bestPrevious.get(currentNode);
			}
		}
		return path;
	}

	/**
	 * Algorithme DFS - vu en cours
	 * 
	 * @param eInit
	 *            - Noeud - Noeud de d�part
	 * @return seq - List<Noeud> - Suite de noeud pour aller du noeud de d�part
	 *         a la configuration finale
	 */
	public static List<Noeud> DFS(Noeud eInit) {
		List<Noeud> seq = new ArrayList<Noeud>();
		HashMap<String, Boolean> visited = new HashMap<>();
		explore(seq, visited, eInit);
		return seq;
	}

	/**
	 * Fonction d'exploration n�cessaire au DFS - vu en cours
	 * 
	 * @param seq
	 *            - List<Noeud> - Liste de noeud qui contiendra le chemin
	 * @param visited
	 *            - List<Noeud> - Liste des noeuds deja visit� par l'algorithme
	 * @param n
	 *            - Noeud - Noeud de lancement de l'algorithme
	 */
	public static void explore(List<Noeud> seq,
			HashMap<String, Boolean> visited, Noeud n) {
		seq.add(n);
		visited.put(n.toString(), true);

		if (!n.isSolution()) {
			n.trouveFils();
			for (Noeud node : n.getFils()) {
				if (!(seq.get(seq.size() - 1)).isSolution()
						&& !visited.containsKey(node.toString())) {
					explore(seq, visited, node);
				}
			}
		}
		if (!(seq.get(seq.size() - 1)).isSolution()) {
			seq.remove(seq.size() - 1);
		}
	}

	/**
	 * M�thode de recherche de solution A*
	 * 
	 * @param eInit
	 *            - Noeud - Noeud initial contenant la configuration de d�part
	 * @return Map<Noeud,Noeud> Map d'etats, on y retrouve le chemin en partant
	 *         du noeud final
	 */
	public static Map<Noeud, Noeud> BFS(Noeud eInit) {
		// Initialisation
		List<Noeud> queue = new ArrayList<Noeud>();
		Map<Noeud, Noeud> pred = new HashMap<Noeud, Noeud>();
		queue.add(eInit);
		graphe.put(eInit.toString(), true);
		pred.put(eInit, null);
		boolean test = false;

		// Bouclage
		while (queue.size() != 0 && !test) {
			Noeud eCurrent = queue.get(0);
			queue.remove(0);
			eCurrent.trouveFils();
			for (Noeud fils : eCurrent.getFils()) {
				if (!graphe.containsKey(fils.toString())) {
					graphe.put(fils.toString(), true);
					queue.add(fils);
					pred.put(fils, eCurrent);
					if (fils.isSolution()) {
						test = true;
						noeudFinale = fils;
					}
				}
			}
		}
		return pred;
	}

}
