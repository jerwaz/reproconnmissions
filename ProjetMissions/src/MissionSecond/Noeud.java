package MissionSecond;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Mod�le de noeud dans le cadre de la r�solution du Taquin Il s'agit d'un
 * mod�le d'arbre standard avec cout o� le nom contient, l'�tat de ses grilles
 * de d�part et d'arriv�e, ses fils, les couts associ�s, l'�tat de l'�quipe de
 * protection, et le nombre de bateau au d�part
 * 
 * @author William
 *
 */
public class Noeud {

	public List<Bateau> depart;
	public List<Bateau> arrivee;
	public List<Noeud> fils;
	public Map<String, Integer> cout;
	public boolean isSecuritePrete = true; // Boolean qui sera a false si
											// l'�quipe doit revenir chercher
											// des bateaux
	int originalBoatNumber;

	/**
	 * Constructeur de la classe
	 * 
	 * @param depart
	 *            - List<Bateau> - Liste des bateaux pr�sents au d�part
	 * @param arrivee
	 *            - List<Bateau> - Liste des bateaux pr�sents � l'arriv�
	 * @param fils
	 *            - List<Noeud> - Liste des fils du bateau
	 */
	public Noeud(List<Bateau> depart, List<Bateau> arrivee, List<Noeud> fils) {
		super();
		this.depart = depart;
		this.arrivee = arrivee;
		this.fils = fils;
		this.cout = new HashMap<>();
		originalBoatNumber = depart.size() + arrivee.size();
	}

	/**
	 * Fonction de g�n�ration des fils du noeud courant. Dans le cas ligne de
	 * d�part : toutes les combinaisons de deux parties n bateaux sur la ligne
	 * de d�part sont stock�s comme des fils du noeud courant Dans le cas du
	 * retour : Tous les retours de bateaux sont stock�s comme des fils du noeud
	 * courant L'alternance des cas est g�r� par le boolean attribut
	 */
	public void trouveFils() {
		// Cas du d�part et double boucle de parcours
		if (isSecuritePrete) {
			for (int i = 0; i < depart.size(); i++) {
				for (int j = i + 1; j < depart.size(); j++) {
					List<Bateau> departFils = new ArrayList<>(depart);
					List<Bateau> arriveeFils = new ArrayList<>(arrivee);
					List<Noeud> filsFils = new ArrayList<>();
					Bateau toRemoveFirst = departFils.get(i);
					Bateau toRemoveSecond = depart.get(j);
					arriveeFils.add(toRemoveFirst);
					arriveeFils.add(toRemoveSecond);
					departFils.remove(toRemoveFirst);
					departFils.remove(toRemoveSecond);
					Noeud fils = new Noeud(departFils, arriveeFils, filsFils);
					// La cout du trajet sera �gal a la dur�e la plus longue des
					// deux bateaux
					if (toRemoveFirst.getDuree() > toRemoveSecond.getDuree()) {
						cout.put(fils.toString(), toRemoveFirst.getDuree());
					} else {
						cout.put(fils.toString(), toRemoveSecond.getDuree());
					}
					this.fils.add(fils);
				}
			}

		}
		// Cas du retour et boucle simple de parcours
		else {
			for (Bateau b : arrivee) {
				List<Bateau> departFils = new ArrayList<>(depart);
				List<Bateau> arriveeFils = new ArrayList<>(arrivee);
				List<Noeud> filsFils = new ArrayList<>();
				departFils.add(b);
				arriveeFils.remove(b);
				Noeud fils = new Noeud(departFils, arriveeFils, filsFils);
				cout.put(fils.toString(), b.getDuree());
				this.fils.add(fils);
			}
		}
		// Inversion du bouleen pour tous les fils du noeud
		for (Noeud n : fils) {
			n.setSecuritePrete(!isSecuritePrete);
		}
	}

	/**
	 * Fonction de v�rification d'�galit� faible � la fin Elle ne servira que
	 * pour v�rifier que la configuration finale est atteinte en v�rifiant que
	 * tous les bateaux sont bien dans la liste d'arriv�e
	 * 
	 * @param toMatchDepart
	 *            - List<Bateau> - Liste des bateaux au d�part
	 * @param toMatchArrivee
	 *            - List<Bateau> - Liste des bateaux a l'arriv�e
	 * @return test - boolean - Etat de l'�galit�
	 */
	public boolean equality(List<Bateau> toMatchDepart, List<Bateau> toMatchArrivee) {
		boolean test = false;

		if (arrivee.size() == toMatchArrivee.size()) {
			test = true;
		}
		return test;
	}

	/**
	 * Fonction de v�rification d'�galit� faible au d�marrage Elle ne servira
	 * que pour v�rifier que la configuration initiale est atteinte en v�rifiant
	 * que tous les bateaux sont bien dans la liste de d�part
	 * 
	 * @param toMatchDepart
	 *            - List<Bateau> - Liste des bateaux au d�part
	 * @param toMatchArrivee
	 *            - List<Bateau> - Liste des bateaux a l'arriv�e
	 * @return test - boolean - Etat de l'�galit�
	 */
	public boolean equalityInit(List<Bateau> toMatchDepart, List<Bateau> toMatchArrivee) {
		boolean test = false;

		if (depart.size() == toMatchDepart.size()) {
			test = true;
		}

		return test;
	}

	/**
	 * Fonction d'affichage de l'�tat du noeud : affiche les deux grilles dans
	 * la console
	 */
	public String toString() {
		String result = "\n";
		for (int i = 0; i < originalBoatNumber; i++) {
			if (i < depart.size()) {
				result += depart.get(i).getName();
			} else {
				result += "null";
			}
			result += "\n";
		}
		for (int i = 0; i < originalBoatNumber; i++) {
			if (i < arrivee.size()) {
				result += arrivee.get(i).getName();
			} else {
				result += "null";
			}
			result += "\n";
		}
		return result;

	}

	// Les fonctions qui suivent cette ligne sont toutes des getters/setters des
	// diff�rents attributs de cette classe

	public List<Bateau> getDepart() {
		return depart;
	}

	public void setDepart(List<Bateau> depart) {
		this.depart = depart;
	}

	public List<Bateau> getArrivee() {
		return arrivee;
	}

	public void setArrivee(List<Bateau> arrivee) {
		this.arrivee = arrivee;
	}

	public boolean isSecuritePrete() {
		return isSecuritePrete;
	}

	public void setSecuritePrete(boolean isSecuritePrete) {
		this.isSecuritePrete = isSecuritePrete;
	}

	public List<Noeud> getFils() {
		return fils;
	}

	public void setFils(List<Noeud> fils) {
		this.fils = fils;
	}

	public Map<String, Integer> getCout() {
		return cout;
	}

	public void setCout(Map<String, Integer> cout) {
		this.cout = cout;
	}

}
