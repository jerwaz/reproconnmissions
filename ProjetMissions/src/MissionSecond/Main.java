package MissionSecond;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe principale de ce sous-projet Elle impl�mente la m�thode de lancement
 * pourvu de diff�rents jeux d'exemples (comment� ou d�comment les lignes) Elle
 * impl�mente par ailleurs les diff�rents algorithmes de parcours de graphe pour
 * la r�solution du probl�me ATTENTION : Une erreur stackOverflow est possible
 * si la m�moire de votre VM n'est pas suffisante
 * 
 * @author William
 *
 */
public class Main {

	public static Noeud init;
	public static Noeud finale;
	public static Map<String, Boolean> graphe = new HashMap<String, Boolean>();
	public static Noeud BFSEndpoint;
	public static Noeud AEndpoint;

	/**
	 * M�thode de lancement du programme, elle donne les r�sultats des trois
	 * algorithmes de parcours et le nombre d'�tape dans le chemin. Pour
	 * effectuer des tests, il suffit de cr�er ou supprimer des bateaux et de
	 * les ajouter a la liste de d�part
	 * 
	 * @param args
	 *            - String[] - Arguments standard de la m�thode main
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Initialisation du board initiales de bateau
		Bateau boat1 = new Bateau("XC21", 45);
		Bateau boat2 = new Bateau("XC56", 90);
		Bateau boat3 = new Bateau("XC100", 225);
		Bateau boat4 = new Bateau("XC800", 360);
		//Bateau boat5 = new Bateau("XCTEST", 180);
		//Bateau boat6 = new Bateau("XCTEST2", 420);
		//Bateau boat7 = new Bateau("Blabla", 25);
		List<Bateau> departInit = new ArrayList<>();
		List<Bateau> arriveInit = new ArrayList<>();
		List<Noeud> initFils = new ArrayList<>();
		departInit.add(boat1);
		departInit.add(boat2);
		departInit.add(boat3);
		departInit.add(boat4);
		//departInit.add(boat5);
		//departInit.add(boat6);
		//departInit.add(boat7);
		init = new Noeud(departInit, arriveInit, initFils);
		List<Bateau> departFinale = new ArrayList<>();
		List<Bateau> arriveFinale = new ArrayList<>();
		List<Noeud> finaleFils = new ArrayList<>();
		arriveFinale.add(boat1);
		arriveFinale.add(boat2);
		arriveFinale.add(boat3);
		arriveFinale.add(boat4);
		//arriveFinale.add(boat5);
		//arriveFinale.add(boat6);
		//arriveFinale.add(boat7);
		finale = new Noeud(departFinale, arriveFinale, finaleFils);

		// R�cup�ration et affichage du chemin pour le BFS
		long BFSexectic = System.currentTimeMillis();
		Map<Noeud, Noeud> chemin = BFS(init);
		long BFSexectoc = System.currentTimeMillis() - BFSexectic;
		Noeud noeudTemp = BFSEndpoint;
		boolean test = true;
		int IterationNumber = 0;
		int coutHorraire = 0;
		while (test) {
			if (noeudTemp.equals(init)) {
				test = false;
			} else {
				IterationNumber += 1;
			}
			Noeud toCalculateCost = noeudTemp;
			noeudTemp = chemin.get(noeudTemp);
			if (!toCalculateCost.equals(init)) {
				coutHorraire += noeudTemp.getCout().get(toCalculateCost.toString());
			}
		}
		System.out.println("BFS - Nombre d'�tapes : " + IterationNumber);
		System.out.println("BFS - Dur�e de la mission : " + coutHorraire);
		System.out.println("BFS  - Temps d'�x�cution : " + BFSexectoc+" millisecondes");

		// R�cup�ration et affichage du chemin pour le DFS
		long DFSexectic = System.currentTimeMillis();
		List<Noeud> chemin2 = DFS(init);
		long DFSexectoc = System.currentTimeMillis() - DFSexectic;
		System.out.println("DFS - Nombre d'�tapes : " + (chemin2.size() - 1));
		int coutHeureDFS = 0;
		for (int i = 0; i < chemin2.size() - 2; i++) {
			coutHeureDFS += chemin2.get(i).getCout().get(chemin2.get(i + 1).toString());
		}
		coutHeureDFS += chemin2.get(chemin2.size()-2).getCout().get(chemin2.get(chemin2.size()-1).toString());
		System.out.println("DFS - Dur�e de la mission : " + (coutHeureDFS));
		System.out.println("DFS - Temps d'�x�cution : " + DFSexectoc+" millisecondes");

		// R�cup�ration et affichage du chemin pour le A*
		long Aexectic = System.currentTimeMillis();
		List<Noeud> chemin3 = Astar(init, finale);
		long Aexectoc = System.currentTimeMillis() - Aexectic;
		System.out.println("A* - Nombre d'�tapes : " + chemin3.size());
		int coutHeureA = 0;
		for (int i = chemin3.size() - 2; i >= 0; i--) {
			coutHeureA += chemin3.get(i + 1).getCout().get(chemin3.get(i).toString());
		}
		
		System.out.println("A* - Dur�e de la mission : " + (coutHeureA + chemin3.get(0).getCout().get(AEndpoint.toString())));
		System.out.println("A* - Temps d'�x�cution : " + Aexectoc+" millisecondes");
	}

	/**
	 * Algorithme A* - Vu en cour - Le seul ajout est la m�thode permettant de
	 * trouver les fils d'un noeud
	 * 
	 * @param eInit
	 *            - Noeud - Noeud initial contenant la configuration de d�part
	 * @param eFinale
	 *            - Noeud - Noeud finale contenant la configuration cible
	 * @return
	 */
	public static List<Noeud> Astar(Noeud eInit, Noeud eFinale) {
		List<Noeud> ongoing = new ArrayList<Noeud>();
		ongoing.add(eInit);
		Map<Noeud, Integer> fCost = new HashMap<>();
		Map<String, Integer> lowestCostTo = new HashMap<>();
		lowestCostTo.put(eInit.toString(), 0);
		Map<Noeud, Noeud> bestPrevious = new HashMap<>();
		bestPrevious.put(eInit, null);
		List<Noeud> toReturn = new ArrayList<>();

		while (!ongoing.isEmpty() && toReturn.isEmpty()) {
			Noeud bestNode = minimize_f(ongoing, fCost);
			if (bestNode.equality(eFinale.getDepart(), eFinale.getArrivee())) {
				toReturn = analyse_f(bestPrevious, bestNode);
				AEndpoint = bestNode;
			}
			bestNode.trouveFils();
			ongoing.remove(bestNode);
			for (Noeud n : bestNode.getFils()) {
				int newCostToN = lowestCostTo.get(bestNode.toString()) + cost(bestNode, n);
				if (!lowestCostTo.containsKey(n.toString())) {
					lowestCostTo.put(n.toString(), newCostToN);
					bestPrevious.put(n, bestNode);
					fCost.put(n, lowestCostTo.get(n.toString()) + heuristique(n, eFinale));
					ongoing.add(n);
				} else if (newCostToN < lowestCostTo.get(n.toString())) {
					lowestCostTo.replace(n.toString(), newCostToN);
					bestPrevious.replace(n, bestNode);
					fCost.replace(n, lowestCostTo.get(n.toString()) + heuristique(n, eFinale));
				}
			}
		}
		return toReturn;
	}

	/**
	 * Fonction n�cessaire � l'A* pour trouver le Noeud dont le cout est le
	 * moins �l�v�
	 * 
	 * @param ongoing
	 *            - List<Noeud> - Listes des noeuds a contr�ler
	 * @param fCost
	 *            - Map<Noeud,Integer> - Liste des couts associ�s
	 * @return bestNode - Noeud - Noeud de cout le moins �l�v�
	 */
	public static Noeud minimize_f(List<Noeud> ongoing, Map<Noeud, Integer> fCost) {
		Noeud bestNode = ongoing.get(0);
		if (ongoing.size() != 1) {
			for (Noeud n : ongoing) {
				if (fCost.get(n) < fCost.get(bestNode)) {
					bestNode = n;
				}
			}
		}
		return bestNode;
	}

	/**
	 * Fonction n�cessaire a l'A* renvoyant le cour entre deux noeuds
	 * 
	 * @param bestNode
	 *            - Noeud de cout le moins elev�
	 * @param n
	 *            - Noeud source
	 * @return cost - int - Cout de parcours
	 */
	public static int cost(Noeud bestNode, Noeud n) {
		int cost = bestNode.getCout().get((n).toString());
		return cost;
	}

	/**
	 * Fonction n�cessaire � l'A* retrouvant le chemin pour atteindre un noeud
	 * donn�
	 * 
	 * @param bestPrevious
	 *            - Map<Noeud,Noeud> - Map des noeuds associ�s a leur meilleure
	 *            pr�decesseur
	 * @param bestNode
	 *            - Noeud - Le Noeud de d�part de l'analyse
	 * @return List<Noeud> - Chemin pour atteindre le "bestNode"
	 */
	public static List<Noeud> analyse_f(Map<Noeud, Noeud> bestPrevious, Noeud bestNode) {
		List<Noeud> path = new ArrayList<>();
		Noeud currentNode = bestNode;
		boolean test = false;
		while (!test) {
			if (bestPrevious.get(currentNode) == null) {
				test = true;
			} else {
				path.add(bestPrevious.get(currentNode));
				currentNode = bestPrevious.get(currentNode);
			}
		}
		return path;
	}

	/**
	 * Heuristique n�cessaire � l'A*. Elle renverra soit le produit du nombre de
	 * bateau restant sur la ligne de d�part et de la plus petite dur�e de
	 * travers�e pour l'aller, soit la dur�e la plus courte du bateau sur la
	 * ligne d'arriv� pour le retour	 * 
	 * @param n
	 *            - Noeud - Noeud courant
	 * @param efinale
	 *            - Noeud - Noeud cible
	 * @return mismatch - int - Nombre de case mal plac�es
	 */
	public static int heuristique(Noeud n, Noeud efinale) {
		int heur = 999999999;
		if (n.isSecuritePrete) {
			List<Bateau> remainingBoat = n.getDepart();
			for (Bateau b : remainingBoat) {
				heur = Math.min(heur, b.getDuree() * remainingBoat.size());
			}
		} else {
			List<Bateau> remainingBoat = n.getArrivee();
			for (Bateau b : remainingBoat) {
				heur = Math.min(heur, b.getDuree());
			}
		}
		return heur;
	}

	/**
	 * Algorithme DFS - vu en cours Notons que la configuration finale est donn�
	 * par l'attribut du d�but de la classe et non par un param�tre dans ce cas
	 * !
	 * 
	 * @param eInit
	 *            - Noeud - Noeud de d�part
	 * @return seq - List<Noeud> - Suite de noeud pour aller du noeud de d�part a
	 *         la configuration finale
	 */
	public static List<Noeud> DFS(Noeud eInit) {
		List<Noeud> seq = new ArrayList<Noeud>();
		HashMap<String, Boolean> visited = new HashMap<>();
		explore(seq, visited, eInit);
		return seq;
	}

	/**
	 * Fonction d'exploration n�cessaire au DFS - vu en cours
	 * 
	 * @param seq
	 *            - List<Noeud> - Liste de noeud qui contiendra le chemin
	 * @param visited
	 *            - List<Noeud> - Liste des noeuds deja visit� par l'algorithme
	 * @param n
	 *            - Noeud - Noeud de lancement de l'algorithme
	 */
	public static void explore(List<Noeud> seq, HashMap<String, Boolean> visited, Noeud n) {
		seq.add(n);
		visited.put(n.toString(), true);
		if (!n.equality(finale.getDepart(), finale.getArrivee())) {
			n.trouveFils();
			for (Noeud node : n.getFils()) {
				if (!(seq.get(seq.size() - 1)).equality(finale.getDepart(), finale.getArrivee())
						&& !visited.containsKey(node.toString())) {
					explore(seq, visited, node);
				}
			}
		}
		if (!(seq.get(seq.size() - 1)).equality(finale.getDepart(), finale.getArrivee())) {
			seq.remove(seq.size() - 1);
		}
	}

	/**
	 * Fonction d'exploration n�cessaire au DFS - vu en cours
	 * 
	 * @param seq
	 *            - List<Noeud> - Liste de noeud qui contiendra le chemin
	 * @param visited
	 *            - List<Noeud> - Liste des noeuds deja visit� par l'algorithme
	 * @param n
	 *            - Noeud - Noeud de lancement de l'algorithme
	 */
	public static Map<Noeud, Noeud> BFS(Noeud eInit) {
		// Initialisation
		List<Noeud> queue = new ArrayList<Noeud>();
		Map<Noeud, Noeud> pred = new HashMap<Noeud, Noeud>();
		queue.add(eInit);
		graphe.put(eInit.toString(), true);
		pred.put(eInit, null);
		boolean test = false;

		// Bouclage
		while (queue.size() != 0 && !test) {
			Noeud eCurrent = queue.get(0);
			queue.remove(0);
			eCurrent.trouveFils();
			for (Noeud fils : eCurrent.getFils()) {
				// if(!graphe.containsKey(fils.toString())){ //UNACTIVATE
				// FILTERING FOR RANGE > 6, currenty OutOfMemory for 7
				graphe.put(fils.toString(), true);
				queue.add(fils);
				pred.put(fils, eCurrent);
				if (fils.equality(finale.getDepart(), finale.getArrivee())) {
					test = true;
					BFSEndpoint = fils;
				}
				// }
			}
		}
		return pred;
	}
}
