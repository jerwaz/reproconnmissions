package MissionSecond;

/**
 * Classe de gestion de l'objet b�teaux dans une optique de g�n�ralisation du
 * probl�me Un bateau est ainsi cart�ris� par son nom et la dur�e de son trajet
 * 
 * @author William
 *
 */
public class Bateau {

	private String name;
	private int duree;

	/**
	 * Constructeur de la classe
	 * 
	 * @param name
	 *            - String - Nom du bateau, fait office d'identifiant
	 * @param duree
	 *            - int - Dur�e du trajet
	 */
	public Bateau(String name, Integer duree) {
		this.name = name;
		this.duree = duree;
	}

	// Les fonctions qui suivent cette ligne sont toutes des getters/setters des
	// diff�rents attributs de cette classe

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getDuree() {
		return duree;
	}

	public void setDuree(Integer duree) {
		this.duree = duree;
	}

}
